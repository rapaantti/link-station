const expect     = require('chai').expect;
const calculator = require('../src/calculator');

describe('calculator module', () => {
    describe('findMostSuitableStation', () => {
        it('should find expected station', () => {
            const tests = [
                {
                    stations: [[0, 0, 10]],
                    device:   [0, 0],
                    expected: 'Best link station for point 0, 0 is 0, 0 with power 10'
                },
                {
                    stations: [[0, 0, 10], [10, 10, 1]],
                    device:   [10, 10],
                    expected: 'Best link station for point 10, 10 is 10, 10 with power 1'
                },
                {
                    stations: [[-1, -1, 3], [10, 10, 1]],
                    device:   [1, 1],
                    expected: 'Best link station for point 1, 1 is -1, -1 with power 3'
                },
            ];

            for (let i = 0; i < tests.length; i++) {
                expect(calculator.findMostSuitableStation(tests[i].stations, tests[i].device))
                    .to
                    .equal(tests[i].expected);
            }
        });

        it('should not find any stations', () => {
            const tests = [
                {
                    stations: [[0, 0, 10]],
                    device:   [11, 11],
                    expected: 'No link station within reach for point 11, 11'
                },
                {
                    stations: [[0, 0, 10], [10, 10, 1]],
                    device:   [-20, 10],
                    expected: 'No link station within reach for point -20, 10'
                },
                {
                    stations: [[-1, -1, 3], [10, 10, 1]],
                    device:   [100, 100],
                    expected: 'No link station within reach for point 100, 100'
                },
            ];

            for (let i = 0; i < tests.length; i++) {
                expect(calculator.findMostSuitableStation(tests[i].stations, tests[i].device))
                    .to
                    .equal(tests[i].expected);
            }
        });
    });
});