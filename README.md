# Requirements:
* nodejs (tested with 7.8.0)
* npm (tested with 4.2.0)

# Installation

Run following command on repository root directory after clone:

```
npm install
```

# Executing

Run following command to execute the script:
```
npm start
```
# Tests

Run all unit tests with following command:
```
npm test
```