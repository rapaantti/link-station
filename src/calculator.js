/**
 * Finds most suitable station for given device.
 *
 * @param {Array} stations
 * @param {Array} device
 *
 * @returns {string}
 */
function findMostSuitableStation(stations, device) {
    let maxPower = 0;
    let station  = null;
    let power    = 0;

    for (let i = 0; i < stations.length; i++) {
        power = calculatePower(stations[i], device);

        if (maxPower < power) {
            maxPower = power;
            station = stations[i];
        }
    }

    return station ?
        `Best link station for point ${device[0]}, ${device[1]} is ${station[0]}, ${station[1]} with power ${station[2]}`:
        `No link station within reach for point ${device[0]}, ${device[1]}`;
}

/**
 * Calculates power between station and device.
 *
 * @param {Array} station
 * @param {Array} device
 *
 * @returns {number}
 */
function calculatePower(station, device) {
    let distance = Math.sqrt(Math.pow(station[0] - device[0], 2) + Math.pow(station[1] - device[1], 2));
    return distance > station[2] ? 0 : Math.pow(station[2] - distance, 2);
}

module.exports = {
    findMostSuitableStation: findMostSuitableStation
};