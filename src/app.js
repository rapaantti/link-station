const calculator = require('./calculator');

// Task variables
const devices  = [[0, 0], [100, 100], [15, 10], [18, 18]];
const stations = [[0, 0, 10], [20, 20, 5], [10, 0, 12]];

// Print out actual task results
for (let i = 0; i < devices.length; i++) {
    console.log(calculator.findMostSuitableStation(stations, devices[i]));
}
